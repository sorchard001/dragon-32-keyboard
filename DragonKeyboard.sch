EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Switch:SW_Push_45deg SW1
U 1 1 60952825
P 3850 2100
F 0 "SW1" H 3950 2250 50  0000 C CNN
F 1 "0" H 4000 2100 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 3850 2100 50  0001 C CNN
F 3 "~" H 3850 2100 50  0001 C CNN
	1    3850 2100
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push_45deg SW8
U 1 1 609538EF
P 4250 2100
F 0 "SW8" H 4350 2250 50  0000 C CNN
F 1 "1" H 4400 2100 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 4250 2100 50  0001 C CNN
F 3 "~" H 4250 2100 50  0001 C CNN
	1    4250 2100
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push_45deg SW15
U 1 1 60953E93
P 4650 2100
F 0 "SW15" H 4750 2250 50  0000 C CNN
F 1 "2" H 4800 2100 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 4650 2100 50  0001 C CNN
F 3 "~" H 4650 2100 50  0001 C CNN
	1    4650 2100
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push_45deg SW22
U 1 1 609545B8
P 5050 2100
F 0 "SW22" H 5150 2250 50  0000 C CNN
F 1 "3" H 5200 2100 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 5050 2100 50  0001 C CNN
F 3 "~" H 5050 2100 50  0001 C CNN
	1    5050 2100
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push_45deg SW28
U 1 1 60954C58
P 5450 2100
F 0 "SW28" H 5550 2250 50  0000 C CNN
F 1 "4" H 5600 2100 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 5450 2100 50  0001 C CNN
F 3 "~" H 5450 2100 50  0001 C CNN
	1    5450 2100
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push_45deg SW34
U 1 1 609551C7
P 5850 2100
F 0 "SW34" H 5950 2250 50  0000 C CNN
F 1 "5" H 6000 2100 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 5850 2100 50  0001 C CNN
F 3 "~" H 5850 2100 50  0001 C CNN
	1    5850 2100
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push_45deg SW40
U 1 1 60955638
P 6250 2100
F 0 "SW40" H 6350 2250 50  0000 C CNN
F 1 "6" H 6400 2100 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 6250 2100 50  0001 C CNN
F 3 "~" H 6250 2100 50  0001 C CNN
	1    6250 2100
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push_45deg SW46
U 1 1 60955B2A
P 6650 2100
F 0 "SW46" H 6750 2250 50  0000 C CNN
F 1 "7" H 6800 2100 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 6650 2100 50  0001 C CNN
F 3 "~" H 6650 2100 50  0001 C CNN
	1    6650 2100
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push_45deg SW2
U 1 1 609594EE
P 3850 2500
F 0 "SW2" H 3950 2650 50  0000 C CNN
F 1 "8" H 4000 2500 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 3850 2500 50  0001 C CNN
F 3 "~" H 3850 2500 50  0001 C CNN
	1    3850 2500
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push_45deg SW9
U 1 1 609594F8
P 4250 2500
F 0 "SW9" H 4350 2650 50  0000 C CNN
F 1 "9" H 4400 2500 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 4250 2500 50  0001 C CNN
F 3 "~" H 4250 2500 50  0001 C CNN
	1    4250 2500
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push_45deg SW16
U 1 1 60959502
P 4650 2500
F 0 "SW16" H 4750 2650 50  0000 C CNN
F 1 ":" H 4800 2500 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 4650 2500 50  0001 C CNN
F 3 "~" H 4650 2500 50  0001 C CNN
	1    4650 2500
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push_45deg SW23
U 1 1 6095950C
P 5050 2500
F 0 "SW23" H 5150 2650 50  0000 C CNN
F 1 ";" H 5200 2500 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 5050 2500 50  0001 C CNN
F 3 "~" H 5050 2500 50  0001 C CNN
	1    5050 2500
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push_45deg SW29
U 1 1 60959516
P 5450 2500
F 0 "SW29" H 5550 2650 50  0000 C CNN
F 1 "," H 5600 2500 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 5450 2500 50  0001 C CNN
F 3 "~" H 5450 2500 50  0001 C CNN
	1    5450 2500
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push_45deg SW35
U 1 1 60959520
P 5850 2500
F 0 "SW35" H 5950 2650 50  0000 C CNN
F 1 "-" H 6000 2500 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 5850 2500 50  0001 C CNN
F 3 "~" H 5850 2500 50  0001 C CNN
	1    5850 2500
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push_45deg SW41
U 1 1 6095952A
P 6250 2500
F 0 "SW41" H 6350 2650 50  0000 C CNN
F 1 "." H 6400 2500 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 6250 2500 50  0001 C CNN
F 3 "~" H 6250 2500 50  0001 C CNN
	1    6250 2500
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push_45deg SW47
U 1 1 60959534
P 6650 2500
F 0 "SW47" H 6750 2650 50  0000 C CNN
F 1 "/" H 6800 2500 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 6650 2500 50  0001 C CNN
F 3 "~" H 6650 2500 50  0001 C CNN
	1    6650 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6750 2200 6350 2200
Wire Wire Line
	6350 2200 5950 2200
Connection ~ 6350 2200
Wire Wire Line
	5950 2200 5550 2200
Connection ~ 5950 2200
Wire Wire Line
	5550 2200 5150 2200
Connection ~ 5550 2200
Wire Wire Line
	5150 2200 4750 2200
Connection ~ 5150 2200
Wire Wire Line
	4750 2200 4350 2200
Connection ~ 4750 2200
Wire Wire Line
	4350 2200 3950 2200
Connection ~ 4350 2200
Wire Wire Line
	6750 2600 6350 2600
Wire Wire Line
	6350 2600 5950 2600
Connection ~ 6350 2600
Wire Wire Line
	5950 2600 5550 2600
Connection ~ 5950 2600
Wire Wire Line
	5550 2600 5150 2600
Connection ~ 5550 2600
Wire Wire Line
	5150 2600 4750 2600
Connection ~ 5150 2600
Wire Wire Line
	4750 2600 4350 2600
Connection ~ 4750 2600
Wire Wire Line
	4350 2600 3950 2600
Connection ~ 4350 2600
Wire Wire Line
	6550 2000 6550 2400
Wire Wire Line
	6150 2000 6150 2400
Wire Wire Line
	5750 2000 5750 2400
Wire Wire Line
	5350 2000 5350 2400
Wire Wire Line
	4950 2000 4950 2400
Wire Wire Line
	4550 2000 4550 2400
Wire Wire Line
	4150 2000 4150 2400
Wire Wire Line
	3750 2000 3750 2400
$Comp
L Switch:SW_Push_45deg SW3
U 1 1 609A04C6
P 3850 2900
F 0 "SW3" H 3950 3050 50  0000 C CNN
F 1 "@" H 4000 2900 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 3850 2900 50  0001 C CNN
F 3 "~" H 3850 2900 50  0001 C CNN
	1    3850 2900
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push_45deg SW10
U 1 1 609A04D0
P 4250 2900
F 0 "SW10" H 4300 3050 50  0000 C CNN
F 1 "A" H 4400 2900 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 4250 2900 50  0001 C CNN
F 3 "~" H 4250 2900 50  0001 C CNN
	1    4250 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4350 3000 3950 3000
Connection ~ 4350 3000
Connection ~ 6550 2400
Connection ~ 6150 2400
Connection ~ 5750 2400
Connection ~ 5350 2400
Connection ~ 4950 2400
Connection ~ 4550 2400
Wire Wire Line
	4150 2400 4150 2800
Connection ~ 4150 2400
Connection ~ 4150 2800
Wire Wire Line
	3750 2400 3750 2800
Connection ~ 3750 2400
Connection ~ 3750 2800
$Comp
L Switch:SW_Push_45deg SW27
U 1 1 609B913D
P 5050 4100
F 0 "SW27" H 5150 4250 50  0000 C CNN
F 1 "Up" H 5150 4100 50  0000 L CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 5050 4100 50  0001 C CNN
F 3 "~" H 5050 4100 50  0001 C CNN
	1    5050 4100
	1    0    0    -1  
$EndComp
Connection ~ 5150 4200
$Comp
L Connector_Generic:Conn_01x16 J1
U 1 1 60A0560B
P 1600 2950
F 0 "J1" H 1600 3800 50  0000 C CNN
F 1 "Conn_01x16" H 1518 3776 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x16_P2.54mm_Vertical" H 1600 2950 50  0001 C CNN
F 3 "~" H 1600 2950 50  0001 C CNN
	1    1600 2950
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3950 2200 3450 2200
Connection ~ 3950 2200
Wire Wire Line
	3950 2600 3450 2600
Connection ~ 3950 2600
Wire Wire Line
	3950 3000 3450 3000
Connection ~ 3950 3000
Wire Wire Line
	3750 2000 3750 1700
Connection ~ 3750 2000
Wire Wire Line
	4150 2000 4150 1700
Connection ~ 4150 2000
Wire Wire Line
	4550 2000 4550 1700
Connection ~ 4550 2000
Wire Wire Line
	4950 2000 4950 1700
Connection ~ 4950 2000
Wire Wire Line
	5350 2000 5350 1700
Connection ~ 5350 2000
Wire Wire Line
	5750 2000 5750 1700
Connection ~ 5750 2000
Wire Wire Line
	6150 2000 6150 1700
Connection ~ 6150 2000
Wire Wire Line
	6550 2000 6550 1700
Connection ~ 6550 2000
Text Label 3450 2200 0    50   ~ 0
PA0
Text Label 3450 2600 0    50   ~ 0
PA1
Text Label 3450 3000 0    50   ~ 0
PA2
Text Label 3450 3400 0    50   ~ 0
PA3
Text Label 3450 3800 0    50   ~ 0
PA4
Text Label 3450 4200 0    50   ~ 0
PA5
Text Label 3450 4600 0    50   ~ 0
PA6
Text Label 3750 1700 3    50   ~ 0
PB0
Text Label 4150 1700 3    50   ~ 0
PB1
Text Label 4550 1700 3    50   ~ 0
PB2
Text Label 4950 1700 3    50   ~ 0
PB3
Text Label 5350 1700 3    50   ~ 0
PB4
Text Label 5750 1700 3    50   ~ 0
PB5
Text Label 6150 1700 3    50   ~ 0
PB6
Text Label 6550 1700 3    50   ~ 0
PB7
Wire Wire Line
	1800 2250 2100 2250
Wire Wire Line
	1800 2350 2100 2350
Wire Wire Line
	1800 2550 2100 2550
Wire Wire Line
	1800 2650 2100 2650
Wire Wire Line
	1800 2750 2100 2750
Wire Wire Line
	1800 2850 2100 2850
Wire Wire Line
	1800 2950 2100 2950
Wire Wire Line
	1800 3050 2100 3050
Wire Wire Line
	1800 3150 2100 3150
Wire Wire Line
	1800 3250 2100 3250
Wire Wire Line
	1800 3350 2100 3350
Wire Wire Line
	1800 3450 2100 3450
Wire Wire Line
	1800 3550 2100 3550
Wire Wire Line
	1800 3650 2100 3650
Wire Wire Line
	1800 3750 2100 3750
Text Label 1950 2250 0    50   ~ 0
PA0
Text Label 1950 2350 0    50   ~ 0
PA1
Text Label 1950 2550 0    50   ~ 0
PA2
Text Label 1950 2650 0    50   ~ 0
PA3
Text Label 1950 2750 0    50   ~ 0
PA4
Text Label 1950 2850 0    50   ~ 0
PA5
Text Label 1950 2950 0    50   ~ 0
PA6
Text Label 1950 3050 0    50   ~ 0
PB0
Text Label 1950 3150 0    50   ~ 0
PB1
Text Label 1950 3250 0    50   ~ 0
PB2
Text Label 1950 3350 0    50   ~ 0
PB3
Text Label 1950 3450 0    50   ~ 0
PB4
Text Label 1950 3550 0    50   ~ 0
PB5
Text Label 1950 3650 0    50   ~ 0
PB6
Text Label 1950 3750 0    50   ~ 0
PB7
Wire Wire Line
	3450 3400 3950 3400
Wire Wire Line
	4950 2400 4950 2800
Wire Wire Line
	5350 2400 5350 2800
Wire Wire Line
	5750 2400 5750 2800
Wire Wire Line
	6150 2400 6150 2800
Wire Wire Line
	3450 3800 3950 3800
Wire Wire Line
	6550 2400 6550 2800
Wire Wire Line
	3750 2800 3750 3200
Wire Wire Line
	4150 2800 4150 3200
Wire Wire Line
	3450 4200 3950 4200
$Comp
L Switch:SW_Push_45deg SW24
U 1 1 60AA70DD
P 5050 2900
F 0 "SW24" H 5100 3050 50  0000 C CNN
F 1 "C" H 5200 2900 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 5050 2900 50  0001 C CNN
F 3 "~" H 5050 2900 50  0001 C CNN
	1    5050 2900
	1    0    0    -1  
$EndComp
Connection ~ 4950 2800
Wire Wire Line
	4950 2800 4950 3200
Connection ~ 5150 3000
Wire Wire Line
	5150 3000 5550 3000
$Comp
L Switch:SW_Push_45deg SW30
U 1 1 60AA74E7
P 5450 2900
F 0 "SW30" H 5500 3050 50  0000 C CNN
F 1 "D" H 5600 2900 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 5450 2900 50  0001 C CNN
F 3 "~" H 5450 2900 50  0001 C CNN
	1    5450 2900
	1    0    0    -1  
$EndComp
Connection ~ 5350 2800
Wire Wire Line
	5350 2800 5350 3200
Connection ~ 5550 3000
Wire Wire Line
	5550 3000 5950 3000
$Comp
L Switch:SW_Push_45deg SW36
U 1 1 60AA780D
P 5850 2900
F 0 "SW36" H 5900 3050 50  0000 C CNN
F 1 "E" H 6000 2900 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 5850 2900 50  0001 C CNN
F 3 "~" H 5850 2900 50  0001 C CNN
	1    5850 2900
	1    0    0    -1  
$EndComp
Connection ~ 5750 2800
Wire Wire Line
	5750 2800 5750 3200
Connection ~ 5950 3000
Wire Wire Line
	5950 3000 6350 3000
$Comp
L Switch:SW_Push_45deg SW42
U 1 1 60AA7B90
P 6250 2900
F 0 "SW42" H 6300 3050 50  0000 C CNN
F 1 "F" H 6400 2900 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 6250 2900 50  0001 C CNN
F 3 "~" H 6250 2900 50  0001 C CNN
	1    6250 2900
	1    0    0    -1  
$EndComp
Connection ~ 6150 2800
Wire Wire Line
	6150 2800 6150 3200
Connection ~ 6350 3000
Wire Wire Line
	6350 3000 6750 3000
$Comp
L Switch:SW_Push_45deg SW48
U 1 1 60AA7EA3
P 6650 2900
F 0 "SW48" H 6700 3050 50  0000 C CNN
F 1 "G" H 6800 2900 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 6650 2900 50  0001 C CNN
F 3 "~" H 6650 2900 50  0001 C CNN
	1    6650 2900
	1    0    0    -1  
$EndComp
Connection ~ 6550 2800
Wire Wire Line
	6550 2800 6550 3200
$Comp
L Switch:SW_Push_45deg SW4
U 1 1 60AA826D
P 3850 3300
F 0 "SW4" H 3950 3450 50  0000 C CNN
F 1 "H" H 4000 3300 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 3850 3300 50  0001 C CNN
F 3 "~" H 3850 3300 50  0001 C CNN
	1    3850 3300
	1    0    0    -1  
$EndComp
Connection ~ 3750 3200
Wire Wire Line
	3750 3200 3750 3600
Connection ~ 3950 3400
Wire Wire Line
	3950 3400 4350 3400
$Comp
L Switch:SW_Push_45deg SW11
U 1 1 60AA8629
P 4250 3300
F 0 "SW11" H 4350 3450 50  0000 C CNN
F 1 "I" H 4400 3300 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 4250 3300 50  0001 C CNN
F 3 "~" H 4250 3300 50  0001 C CNN
	1    4250 3300
	1    0    0    -1  
$EndComp
Connection ~ 4150 3200
Wire Wire Line
	4150 3200 4150 3600
Connection ~ 4350 3400
Wire Wire Line
	4550 2400 4550 2800
Wire Wire Line
	4350 3400 4750 3400
Wire Wire Line
	4350 3000 4750 3000
$Comp
L Switch:SW_Push_45deg SW17
U 1 1 60AA9CE7
P 4650 2900
F 0 "SW17" H 4700 3050 50  0000 C CNN
F 1 "B" H 4800 2900 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 4650 2900 50  0001 C CNN
F 3 "~" H 4650 2900 50  0001 C CNN
	1    4650 2900
	1    0    0    -1  
$EndComp
Connection ~ 4550 2800
Wire Wire Line
	4550 2800 4550 3200
Connection ~ 4750 3000
Wire Wire Line
	4750 3000 5150 3000
$Comp
L Switch:SW_Push_45deg SW18
U 1 1 60AAA037
P 4650 3300
F 0 "SW18" H 4700 3450 50  0000 C CNN
F 1 "J" H 4800 3300 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 4650 3300 50  0001 C CNN
F 3 "~" H 4650 3300 50  0001 C CNN
	1    4650 3300
	1    0    0    -1  
$EndComp
Connection ~ 4550 3200
Wire Wire Line
	4550 3200 4550 3600
Connection ~ 4750 3400
Wire Wire Line
	4750 3400 5150 3400
$Comp
L Switch:SW_Push_45deg SW25
U 1 1 60AAA488
P 5050 3300
F 0 "SW25" H 5100 3450 50  0000 C CNN
F 1 "K" H 5200 3300 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 5050 3300 50  0001 C CNN
F 3 "~" H 5050 3300 50  0001 C CNN
	1    5050 3300
	1    0    0    -1  
$EndComp
Connection ~ 4950 3200
Wire Wire Line
	4950 3200 4950 3600
Connection ~ 5150 3400
Wire Wire Line
	5150 3400 5550 3400
$Comp
L Switch:SW_Push_45deg SW31
U 1 1 60AAA771
P 5450 3300
F 0 "SW31" H 5500 3450 50  0000 C CNN
F 1 "L" H 5600 3300 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 5450 3300 50  0001 C CNN
F 3 "~" H 5450 3300 50  0001 C CNN
	1    5450 3300
	1    0    0    -1  
$EndComp
Connection ~ 5350 3200
Wire Wire Line
	5350 3200 5350 3600
Connection ~ 5550 3400
Wire Wire Line
	5550 3400 5950 3400
$Comp
L Switch:SW_Push_45deg SW37
U 1 1 60AAAB52
P 5850 3300
F 0 "SW37" H 5900 3450 50  0000 C CNN
F 1 "M" H 6000 3300 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 5850 3300 50  0001 C CNN
F 3 "~" H 5850 3300 50  0001 C CNN
	1    5850 3300
	1    0    0    -1  
$EndComp
Connection ~ 5750 3200
Wire Wire Line
	5750 3200 5750 3600
Connection ~ 5950 3400
Wire Wire Line
	5950 3400 6350 3400
$Comp
L Switch:SW_Push_45deg SW43
U 1 1 60AAAE21
P 6250 3300
F 0 "SW43" H 6300 3450 50  0000 C CNN
F 1 "N" H 6400 3300 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 6250 3300 50  0001 C CNN
F 3 "~" H 6250 3300 50  0001 C CNN
	1    6250 3300
	1    0    0    -1  
$EndComp
Connection ~ 6150 3200
Wire Wire Line
	6150 3200 6150 3600
Connection ~ 6350 3400
Wire Wire Line
	6350 3400 6750 3400
$Comp
L Switch:SW_Push_45deg SW49
U 1 1 60AAB0B8
P 6650 3300
F 0 "SW49" H 6700 3450 50  0000 C CNN
F 1 "O" H 6800 3300 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 6650 3300 50  0001 C CNN
F 3 "~" H 6650 3300 50  0001 C CNN
	1    6650 3300
	1    0    0    -1  
$EndComp
Connection ~ 6550 3200
Wire Wire Line
	6550 3200 6550 3600
$Comp
L Switch:SW_Push_45deg SW5
U 1 1 60AAB2E0
P 3850 3700
F 0 "SW5" H 3950 3850 50  0000 C CNN
F 1 "P" H 4000 3700 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 3850 3700 50  0001 C CNN
F 3 "~" H 3850 3700 50  0001 C CNN
	1    3850 3700
	1    0    0    -1  
$EndComp
Connection ~ 3750 3600
Wire Wire Line
	3750 3600 3750 4000
Connection ~ 3950 3800
Wire Wire Line
	3950 3800 4350 3800
$Comp
L Switch:SW_Push_45deg SW12
U 1 1 60AAB78A
P 4250 3700
F 0 "SW12" H 4350 3850 50  0000 C CNN
F 1 "Q" H 4400 3700 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 4250 3700 50  0001 C CNN
F 3 "~" H 4250 3700 50  0001 C CNN
	1    4250 3700
	1    0    0    -1  
$EndComp
Connection ~ 4150 3600
Wire Wire Line
	4150 3600 4150 4000
Connection ~ 4350 3800
Wire Wire Line
	4350 3800 4750 3800
$Comp
L Switch:SW_Push_45deg SW19
U 1 1 60AABB42
P 4650 3700
F 0 "SW19" H 4750 3850 50  0000 C CNN
F 1 "R" H 4800 3700 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 4650 3700 50  0001 C CNN
F 3 "~" H 4650 3700 50  0001 C CNN
	1    4650 3700
	1    0    0    -1  
$EndComp
Connection ~ 4550 3600
Wire Wire Line
	4550 3600 4550 4000
Connection ~ 4750 3800
Wire Wire Line
	4750 3800 5150 3800
$Comp
L Switch:SW_Push_45deg SW26
U 1 1 60AABD9A
P 5050 3700
F 0 "SW26" H 5150 3850 50  0000 C CNN
F 1 "S" H 5200 3700 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 5050 3700 50  0001 C CNN
F 3 "~" H 5050 3700 50  0001 C CNN
	1    5050 3700
	1    0    0    -1  
$EndComp
Connection ~ 4950 3600
Wire Wire Line
	4950 3600 4950 4000
Connection ~ 5150 3800
Wire Wire Line
	5150 3800 5550 3800
$Comp
L Switch:SW_Push_45deg SW32
U 1 1 60AAC083
P 5450 3700
F 0 "SW32" H 5550 3850 50  0000 C CNN
F 1 "T" H 5600 3700 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 5450 3700 50  0001 C CNN
F 3 "~" H 5450 3700 50  0001 C CNN
	1    5450 3700
	1    0    0    -1  
$EndComp
Connection ~ 5350 3600
Wire Wire Line
	5350 3600 5350 4000
Connection ~ 5550 3800
Wire Wire Line
	5550 3800 5950 3800
$Comp
L Switch:SW_Push_45deg SW38
U 1 1 60AAC324
P 5850 3700
F 0 "SW38" H 5950 3850 50  0000 C CNN
F 1 "U" H 6000 3700 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 5850 3700 50  0001 C CNN
F 3 "~" H 5850 3700 50  0001 C CNN
	1    5850 3700
	1    0    0    -1  
$EndComp
Connection ~ 5750 3600
Wire Wire Line
	5750 3600 5750 4000
Connection ~ 5950 3800
Wire Wire Line
	5950 3800 6350 3800
$Comp
L Switch:SW_Push_45deg SW44
U 1 1 60AAC5B5
P 6250 3700
F 0 "SW44" H 6350 3850 50  0000 C CNN
F 1 "V" H 6400 3700 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 6250 3700 50  0001 C CNN
F 3 "~" H 6250 3700 50  0001 C CNN
	1    6250 3700
	1    0    0    -1  
$EndComp
Connection ~ 6150 3600
Wire Wire Line
	6150 3600 6150 4000
Connection ~ 6350 3800
Wire Wire Line
	6350 3800 6750 3800
$Comp
L Switch:SW_Push_45deg SW50
U 1 1 60AAC8D1
P 6650 3700
F 0 "SW50" H 6750 3850 50  0000 C CNN
F 1 "W" H 6800 3700 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 6650 3700 50  0001 C CNN
F 3 "~" H 6650 3700 50  0001 C CNN
	1    6650 3700
	1    0    0    -1  
$EndComp
Connection ~ 6550 3600
$Comp
L Switch:SW_Push_45deg SW6
U 1 1 60AACD72
P 3850 4100
F 0 "SW6" H 3950 4250 50  0000 C CNN
F 1 "X" H 4000 4100 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 3850 4100 50  0001 C CNN
F 3 "~" H 3850 4100 50  0001 C CNN
	1    3850 4100
	1    0    0    -1  
$EndComp
Connection ~ 3750 4000
Wire Wire Line
	3750 4000 3750 4400
Connection ~ 3950 4200
Wire Wire Line
	3950 4200 4350 4200
$Comp
L Switch:SW_Push_45deg SW13
U 1 1 60AAD35F
P 4250 4100
F 0 "SW13" H 4350 4250 50  0000 C CNN
F 1 "Y" H 4400 4100 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 4250 4100 50  0001 C CNN
F 3 "~" H 4250 4100 50  0001 C CNN
	1    4250 4100
	1    0    0    -1  
$EndComp
Connection ~ 4150 4000
Wire Wire Line
	4150 4000 4150 4400
Connection ~ 4350 4200
Wire Wire Line
	4350 4200 4750 4200
$Comp
L Switch:SW_Push_45deg SW20
U 1 1 60AAD639
P 4650 4100
F 0 "SW20" H 4750 4250 50  0000 C CNN
F 1 "Z" H 4800 4100 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 4650 4100 50  0001 C CNN
F 3 "~" H 4650 4100 50  0001 C CNN
	1    4650 4100
	1    0    0    -1  
$EndComp
Connection ~ 4550 4000
Wire Wire Line
	4550 4000 4550 4400
Connection ~ 4750 4200
Wire Wire Line
	4750 4200 5150 4200
Wire Wire Line
	5150 4200 5550 4200
Wire Wire Line
	3450 4600 3950 4600
$Comp
L Switch:SW_Push_45deg SW33
U 1 1 60AAE296
P 5450 4100
F 0 "SW33" H 5550 4250 50  0000 C CNN
F 1 "Down" H 5550 4100 50  0000 L CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 5450 4100 50  0001 C CNN
F 3 "~" H 5450 4100 50  0001 C CNN
	1    5450 4100
	1    0    0    -1  
$EndComp
Connection ~ 5550 4200
Wire Wire Line
	5550 4200 5950 4200
$Comp
L Switch:SW_Push_45deg SW39
U 1 1 60AAEF34
P 5850 4100
F 0 "SW39" H 5950 4250 50  0000 C CNN
F 1 "Left" H 5950 4100 50  0000 L CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 5850 4100 50  0001 C CNN
F 3 "~" H 5850 4100 50  0001 C CNN
	1    5850 4100
	1    0    0    -1  
$EndComp
Connection ~ 5950 4200
Wire Wire Line
	5950 4200 6350 4200
$Comp
L Switch:SW_Push_45deg SW45
U 1 1 60AAF26E
P 6250 4100
F 0 "SW45" H 6350 4250 50  0000 C CNN
F 1 "Right" H 6350 4100 50  0000 L CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 6250 4100 50  0001 C CNN
F 3 "~" H 6250 4100 50  0001 C CNN
	1    6250 4100
	1    0    0    -1  
$EndComp
Connection ~ 6350 4200
Wire Wire Line
	6350 4200 6750 4200
$Comp
L Switch:SW_Push_45deg SW51
U 1 1 60AAF69A
P 6650 4100
F 0 "SW51" H 6750 4250 50  0000 C CNN
F 1 "Space" H 6750 4100 50  0000 L CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_6.25u_PCB" H 6650 4100 50  0001 C CNN
F 3 "~" H 6650 4100 50  0001 C CNN
	1    6650 4100
	1    0    0    -1  
$EndComp
Connection ~ 6550 4000
Wire Wire Line
	6550 4000 6550 4400
$Comp
L Switch:SW_Push_45deg SW7
U 1 1 60AAFB19
P 3850 4500
F 0 "SW7" H 3950 4650 50  0000 C CNN
F 1 "Enter" H 3950 4500 50  0000 L CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_2.00u_PCB" H 3850 4500 50  0001 C CNN
F 3 "~" H 3850 4500 50  0001 C CNN
	1    3850 4500
	1    0    0    -1  
$EndComp
Connection ~ 3950 4600
Wire Wire Line
	3950 4600 4350 4600
$Comp
L Switch:SW_Push_45deg SW14
U 1 1 60AB01B0
P 4250 4500
F 0 "SW14" H 4350 4650 50  0000 C CNN
F 1 "Clear" H 4350 4500 50  0000 L CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 4250 4500 50  0001 C CNN
F 3 "~" H 4250 4500 50  0001 C CNN
	1    4250 4500
	1    0    0    -1  
$EndComp
Connection ~ 4350 4600
Wire Wire Line
	4350 4600 4750 4600
$Comp
L Switch:SW_Push_45deg SW21
U 1 1 60AB057B
P 4650 4500
F 0 "SW21" H 4750 4650 50  0000 C CNN
F 1 "Break" H 4750 4500 50  0000 L CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 4650 4500 50  0001 C CNN
F 3 "~" H 4650 4500 50  0001 C CNN
	1    4650 4500
	1    0    0    -1  
$EndComp
Connection ~ 4750 4600
$Comp
L Switch:SW_Push_45deg SW52
U 1 1 60AB0881
P 6650 4500
F 0 "SW52" H 6750 4650 50  0000 C CNN
F 1 "Shift" H 6750 4500 50  0000 L CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.50u_PCB" H 6650 4500 50  0001 C CNN
F 3 "~" H 6650 4500 50  0001 C CNN
	1    6650 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	1800 2450 2100 2450
Text Label 1950 2450 0    50   ~ 0
GND
$Comp
L power:GND #PWR0101
U 1 1 60AB7AC7
P 1850 4150
F 0 "#PWR0101" H 1850 3900 50  0001 C CNN
F 1 "GND" H 1855 3977 50  0000 C CNN
F 2 "" H 1850 4150 50  0001 C CNN
F 3 "" H 1850 4150 50  0001 C CNN
	1    1850 4150
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 60AB82C7
P 1850 4150
F 0 "#FLG0101" H 1850 4225 50  0001 C CNN
F 1 "PWR_FLAG" H 1850 4323 50  0000 C CNN
F 2 "" H 1850 4150 50  0001 C CNN
F 3 "~" H 1850 4150 50  0001 C CNN
	1    1850 4150
	1    0    0    -1  
$EndComp
Text Notes 1000 2000 0    50   ~ 0
PIN 1 ORIENTED TOWARDS LEFT OF KEYBOARD
$Comp
L Switch:SW_Push_45deg SW53
U 1 1 6096A92C
P 6650 4850
F 0 "SW53" H 6750 5000 50  0000 C CNN
F 1 "Shift" H 6750 4850 50  0000 L CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.50u_PCB" H 6650 4850 50  0001 C CNN
F 3 "~" H 6650 4850 50  0001 C CNN
	1    6650 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	6550 4400 6550 4750
Connection ~ 6550 4400
Wire Wire Line
	4750 4600 5150 4600
$Comp
L Switch:SW_Push_45deg SW54
U 1 1 60970CAF
P 5050 4500
F 0 "SW54" H 5150 4650 50  0000 C CNN
F 1 "SP1" H 5150 4500 50  0000 L CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.75u_PCB" H 5050 4500 50  0001 C CNN
F 3 "~" H 5050 4500 50  0001 C CNN
	1    5050 4500
	1    0    0    -1  
$EndComp
Connection ~ 5150 4600
Wire Wire Line
	4950 4000 4950 4400
Connection ~ 4950 4000
Wire Wire Line
	6750 4950 7000 4950
Wire Wire Line
	7000 4950 7000 4600
Wire Wire Line
	7000 4600 6750 4600
Connection ~ 6750 4600
Wire Wire Line
	5150 4600 6750 4600
Wire Wire Line
	6550 3600 6550 3900
$Comp
L Connector_Generic:Conn_01x01 MH1
U 1 1 609A7EA7
P 8350 2000
F 0 "MH1" H 8450 2000 50  0000 L CNN
F 1 "Conn_01x01" H 8700 2000 50  0001 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5_Pad" H 8350 2000 50  0001 C CNN
F 3 "~" H 8350 2000 50  0001 C CNN
	1    8350 2000
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 MH2
U 1 1 609A9087
P 8350 2150
F 0 "MH2" H 8450 2150 50  0000 L CNN
F 1 "Conn_01x01" H 8700 2150 50  0001 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5_Pad" H 8350 2150 50  0001 C CNN
F 3 "~" H 8350 2150 50  0001 C CNN
	1    8350 2150
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 MH3
U 1 1 609A9225
P 8350 2300
F 0 "MH3" H 8450 2300 50  0000 L CNN
F 1 "Conn_01x01" H 8700 2300 50  0001 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5_Pad" H 8350 2300 50  0001 C CNN
F 3 "~" H 8350 2300 50  0001 C CNN
	1    8350 2300
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 MH4
U 1 1 609A936F
P 8350 2450
F 0 "MH4" H 8450 2450 50  0000 L CNN
F 1 "Conn_01x01" H 8700 2450 50  0001 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5_Pad" H 8350 2450 50  0001 C CNN
F 3 "~" H 8350 2450 50  0001 C CNN
	1    8350 2450
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 MH5
U 1 1 609A94F8
P 8350 2600
F 0 "MH5" H 8450 2600 50  0000 L CNN
F 1 "Conn_01x01" H 8700 2600 50  0001 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5_Pad" H 8350 2600 50  0001 C CNN
F 3 "~" H 8350 2600 50  0001 C CNN
	1    8350 2600
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 MH6
U 1 1 609A9661
P 8350 2750
F 0 "MH6" H 8450 2750 50  0000 L CNN
F 1 "Conn_01x01" H 8700 2750 50  0001 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5_Pad" H 8350 2750 50  0001 C CNN
F 3 "~" H 8350 2750 50  0001 C CNN
	1    8350 2750
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 MH7
U 1 1 609A980F
P 8350 2900
F 0 "MH7" H 8450 2900 50  0000 L CNN
F 1 "Conn_01x01" H 8700 2900 50  0001 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5_Pad" H 8350 2900 50  0001 C CNN
F 3 "~" H 8350 2900 50  0001 C CNN
	1    8350 2900
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 MH8
U 1 1 609A9993
P 8350 3050
F 0 "MH8" H 8450 3050 50  0000 L CNN
F 1 "Conn_01x01" H 8700 3050 50  0001 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5_Pad" H 8350 3050 50  0001 C CNN
F 3 "~" H 8350 3050 50  0001 C CNN
	1    8350 3050
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 MH9
U 1 1 609A9B11
P 8350 3200
F 0 "MH9" H 8450 3200 50  0000 L CNN
F 1 "Conn_01x01" H 8700 3200 50  0001 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5_Pad" H 8350 3200 50  0001 C CNN
F 3 "~" H 8350 3200 50  0001 C CNN
	1    8350 3200
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 MH10
U 1 1 609A9C8E
P 8350 3350
F 0 "MH10" H 8450 3350 50  0000 L CNN
F 1 "Conn_01x01" H 8700 3350 50  0001 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5_Pad" H 8350 3350 50  0001 C CNN
F 3 "~" H 8350 3350 50  0001 C CNN
	1    8350 3350
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 MH11
U 1 1 609A9E2C
P 8350 3500
F 0 "MH11" H 8450 3500 50  0000 L CNN
F 1 "Conn_01x01" H 8700 3500 50  0001 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5_Pad" H 8350 3500 50  0001 C CNN
F 3 "~" H 8350 3500 50  0001 C CNN
	1    8350 3500
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 MH12
U 1 1 609A9FDA
P 8350 3650
F 0 "MH12" H 8450 3650 50  0000 L CNN
F 1 "Conn_01x01" H 8700 3650 50  0001 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5_Pad" H 8350 3650 50  0001 C CNN
F 3 "~" H 8350 3650 50  0001 C CNN
	1    8350 3650
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 MH13
U 1 1 609AA198
P 8350 3800
F 0 "MH13" H 8450 3800 50  0000 L CNN
F 1 "Conn_01x01" H 8700 3800 50  0001 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5_Pad" H 8350 3800 50  0001 C CNN
F 3 "~" H 8350 3800 50  0001 C CNN
	1    8350 3800
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 MH14
U 1 1 609AA29E
P 8350 3950
F 0 "MH14" H 8450 3950 50  0000 L CNN
F 1 "Conn_01x01" H 8700 3950 50  0001 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5_Pad" H 8350 3950 50  0001 C CNN
F 3 "~" H 8350 3950 50  0001 C CNN
	1    8350 3950
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 MH15
U 1 1 609AA43C
P 8350 4100
F 0 "MH15" H 8450 4100 50  0000 L CNN
F 1 "Conn_01x01" H 8700 4100 50  0001 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5_Pad" H 8350 4100 50  0001 C CNN
F 3 "~" H 8350 4100 50  0001 C CNN
	1    8350 4100
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 MH16
U 1 1 609AA5C0
P 8350 4250
F 0 "MH16" H 8450 4250 50  0000 L CNN
F 1 "Conn_01x01" H 8700 4250 50  0001 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5_Pad" H 8350 4250 50  0001 C CNN
F 3 "~" H 8350 4250 50  0001 C CNN
	1    8350 4250
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 MH17
U 1 1 609AA75E
P 8350 4400
F 0 "MH17" H 8450 4400 50  0000 L CNN
F 1 "Conn_01x01" H 8700 4400 50  0001 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5_Pad" H 8350 4400 50  0001 C CNN
F 3 "~" H 8350 4400 50  0001 C CNN
	1    8350 4400
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 MH18
U 1 1 609AA8AD
P 8350 4550
F 0 "MH18" H 8450 4550 50  0000 L CNN
F 1 "Conn_01x01" H 8700 4550 50  0001 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5_Pad" H 8350 4550 50  0001 C CNN
F 3 "~" H 8350 4550 50  0001 C CNN
	1    8350 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	8150 2000 8150 2150
Connection ~ 8150 2150
Wire Wire Line
	8150 2150 8150 2300
Connection ~ 8150 2300
Wire Wire Line
	8150 2300 8150 2450
Connection ~ 8150 2450
Wire Wire Line
	8150 2450 8150 2600
Connection ~ 8150 2600
Wire Wire Line
	8150 2600 8150 2750
Connection ~ 8150 2750
Wire Wire Line
	8150 2750 8150 2900
Connection ~ 8150 2900
Wire Wire Line
	8150 2900 8150 3050
Connection ~ 8150 3050
Wire Wire Line
	8150 3050 8150 3200
Connection ~ 8150 3200
Wire Wire Line
	8150 3200 8150 3350
Connection ~ 8150 3350
Wire Wire Line
	8150 3350 8150 3500
Connection ~ 8150 3500
Wire Wire Line
	8150 3500 8150 3650
Connection ~ 8150 3650
Wire Wire Line
	8150 3650 8150 3800
Connection ~ 8150 3800
Wire Wire Line
	8150 3800 8150 3950
Connection ~ 8150 3950
Wire Wire Line
	8150 3950 8150 4100
Connection ~ 8150 4100
Wire Wire Line
	8150 4100 8150 4250
Connection ~ 8150 4250
Wire Wire Line
	8150 4250 8150 4400
Connection ~ 8150 4400
Wire Wire Line
	8150 4400 8150 4550
Connection ~ 8150 4550
Wire Wire Line
	8150 4550 8150 4700
$Comp
L power:GND #PWR?
U 1 1 609D7DFD
P 8150 4700
F 0 "#PWR?" H 8150 4450 50  0001 C CNN
F 1 "GND" H 8155 4527 50  0000 C CNN
F 2 "" H 8150 4700 50  0001 C CNN
F 3 "" H 8150 4700 50  0001 C CNN
	1    8150 4700
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push_45deg SW55
U 1 1 609A9E26
P 7050 4100
F 0 "SW55" H 7150 4250 50  0000 C CNN
F 1 "Space_8U" H 7150 4100 50  0000 L CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 7050 4100 50  0001 C CNN
F 3 "~" H 7050 4100 50  0001 C CNN
	1    7050 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6750 4200 7150 4200
Connection ~ 6750 4200
Wire Wire Line
	6950 4000 6950 3900
Wire Wire Line
	6950 3900 6550 3900
Connection ~ 6550 3900
Wire Wire Line
	6550 3900 6550 4000
$EndSCHEMATC
