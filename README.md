## Replacement Keyboard for the Dragon 32

A replacement keyboard pcb for the Dragon 32 based on Cherry MX switches.

This is just a pcb design for now and has not been tested for fit or function.

Getting hold of suitable keycaps is not going to be straightforward. You may have to settle for blanks in a few places or get some custom printed.

The design allows for an 8U spacebar like the Dragon original but these are not commonly available so I've also designed in a standard 6.25U spacebar with an extra 1.75U function key to the left to fill in the gap. I would have liked to have included a number of footprints for different spacebars but these would have resulted in overlapping holes which can add considerable cost to the pcb.

The two shift keys are 1.5U and the enter key is 2U.

The enter key and spacebar have provision for Cherry MX stabilisers.

The board dimensions are based on the original but have been extended slightly at the top and bottom to make room for the new parts.

Cherry keyswitches are much lower profile than the type used in the Dragon. I would suggest using two boards spaced apart approximately 15mm with hex pillars. There are a number of M2.5 holes included for this purpose. This should also make the keyboard much more rigid than a single board.

The top of the Dragon case is inclined at a steeper angle than the keyboard, meaning it is possible the two location holes in the PCB will need to be manually slotted for best fit. Differences in keycap mounting centres will also play a part.

I used an autorouter with this layout to save time. I've manually routed over a hundred boards professionally but it soon became obvious that hand routing this one would be a hideous job. This is because the keys are not physically arranged in the same way as the matrix. Some manual tweaking was still required to encourage the ground fill to reach most places.
